package Render::TextCSV_XS;

use 5.010000;

require Exporter;
use Carp;
use Class::MOP;
use Text::CSV_XS;
use Data::Dumper;
use Moose;
use MooseX::NonMoose;
use Tie::IxHash;
use IO::Zlib;

extends 'Text::CSV_XS';

our $VERSION = '1.01';

has 'infile' => (
    is => 'rw',
    isa => 'Str',
);

has 'textcsv_xs' => (
    is => 'rw',
);

has 'textcsv_args' => (
    is => 'rw',
    isa => 'HashRef',
);

has skip_pattern => (
    is => 'rw',
);

has sep_char => (
    is => 'rw',
);

has '_fh' => (
    is => 'rw',
#    isa => 'FileHandle',
);

sub setup{
    my $self = shift;

    my $fh;
    my $csv = Text::CSV_XS->new($self->textcsv_args);

    if(!$self->_fh && $self->infile !~ m/gz$/){
        open $fh, "<:encoding(utf8)", $self->infile or croak $self->infile." $!";
        $self->_fh($fh);
    }
    elsif(!$self->_fh && $self->infile =~ m/gz$/){
        $fh = IO::Zlib->new($self->infile, 'rb') or croak $self->infile." $!";
        $self->_fh($fh);
    }

    $self->sep_char($self->textcsv_args->{sep_char}) if $self->textcsv_args->{sep_char};
    $self->textcsv_xs($csv);
    $self->set_columns();
    $self->sanitize_names();
}

sub set_columns{
    my $self = shift;
    
    $self->sep_char(",") if !$self->sep_char;
    while (my $row = $self->textcsv_xs->getline ($self->_fh)) {
        my $line = join($self->sep_char, @$row);
        if($line =~ /${\( $self->skip_pattern )}/){
            next;
        }
        else{
            $self->textcsv_xs->column_names($row);
            last;
        }
    }
}

sub sanitize_names{                                                                                                                                                                                  
    my $self = shift;
    
    my @cols = $self->textcsv_xs->column_names();
    my ($index) = grep $cols[$_] =~ m/^ID$/i, 0 .. $#cols;
    $cols[$index] = "TABLE_".$cols[$index] if defined $index;                                                                                                                                        
    
    for(@cols){s/[^\w]/_/g}

    $self->textcsv_xs->column_names(\@cols);
}      


sub BUILDARGS {
    my $class = shift;

    if ( scalar @_ == 1 && ref( $_[0]) ne 'HASH' ) {
        my $arg = $_[0];
        return blessed($arg) ? { infile => $arg } : { infile => $arg };
    }
    return $class->SUPER::BUILDARGS(@_);
}

sub BUILD {
    my $self = shift;

    return;
}

sub before_build {}
sub after_build {}

__PACKAGE__->meta->make_immutable;

1;
__END__

=head1 NAME

Render::TextCSV_XS::Base - Perl extension for blah blah blah

=head1 SYNOPSIS

    package MyTable;

    use Moose;
    extends 'Render::TextCSV_XS::Base';
    
    my $dt = MyTable->new();

    my $file = "/path/to/file";

    my $skip_pattern ='^\\s*##';
    $skip_pattern = "^\s*##";
    $skip_pattern = qr/^\s*##/;

    $dt->infile($file);
    $dt->sep_char("\t");
    $dt->textcsv_args( { allow_loose_quotes => 1, binary => 1, auto_diag => 1, sep_char => "\t"} );
    $dt->skip_pattern($skip_pattern);
    $dt->setup();

=head1 DESCRIPTION

Stub documentation for Render::TextCSV_XS::Base, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head1 Tags

skip_pattern

Pattern to filter the file by. For now it is a string. Would like to extend to a RegexpRef.
my $skip_pattern ='^\\s*##' || $skip_pattern = "^\s*##"; || $skip_pattern = qr/^\s*##/;

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Jillian Rowe, E<lt>jillian@localdomainE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
